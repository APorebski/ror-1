## Adrian Porębski (zaliczenie)

| Wersja Ruby   | Wersja Rails  |                Baza danych                 | Framework |
| ------------- |:-------------:|:-------------------------------------------|:---------:|
| 2.4.0        | 5.0.2         | SQLite / PostgreSQL| Bootstrap |


[link do aplikacji](https://agile-hamlet-90522.herokuapp.com/)

### Temat: Space Database

Space Database umożliwia dodawanie nowych systemów planetarnych do bazy danych. System może
zawierać takie informacje jak nazwa, wiek, opis oraz poglądowe zdjęcie. Aby dodać bądź edytować wcześniej przez siebie dodany system, należy założyć konto. Dodanie systemu uprawnia
do jego edycji bądź usunięcia.

Login: admin@admin.pl
Hasło: adminadmin


### Gemy:
- carrierwave: gem pozwalając dodawać grafiki
- mini_magick: gem służący do skalowania obrazków
- md_simple_editor: edytor tekstowy pozwalający w bardziej zaawansowany sposób edytować opisy
- cloudiary: hostowanie obrazków