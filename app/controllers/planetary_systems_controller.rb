class PlanetarySystemsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @planetarySystems = PlanetarySystem.all
  end

  def show
    @planetarySystem = PlanetarySystem.find(params[:id])
  end

  def new
    @planetarySystem = current_user.planetary_systems.build
  end

  def edit
    @planetarySystem = PlanetarySystem.find(params[:id])
    if current_user.id != @planetarySystem.user.id
      redirect_to root_path
    end
  end

  def removeImage
    @planetarySystem = PlanetarySystem.find(params[:id])
    @planetarySystem.remove_image!
    @planetarySystem.save
    redirect_to @planetarySystem
  end

  def create
    @planetarySystem = current_user.planetary_systems.build(system_params)

    if @planetarySystem.save
      flash[:notice] = 'Planetary system added!'
      redirect_to @planetarySystem
    else
      flash[:error] = 'Failed to add planetary system!'
      render 'new'
    end
  end

  def update
    @planetarySystem = PlanetarySystem.find(params[:id])

    if @planetarySystem.update(system_params)
      flash[:notice] = 'Planetary system updated!'
      redirect_to @planetarySystem
    else
      flash[:error] = 'Failed to edit planetary system!'
      render 'edit'
    end
  end

  def destroy
    @planetarySystem = PlanetarySystem.find(params[:id])
    if current_user.id != @planetarySystem.user.id
      redirect_to root_path
    end

    if @planetarySystem.destroy
      flash[:notice] = 'Planetary system deleted!'
      redirect_to planetary_systems_path
    else
      flash[:error] = 'Failed to delete this planetary system!'
      render :destroy
    end
  end

  private
    def system_params
      params.require(:planetary_system).permit(:name, :age, :image, :description)
    end
end
