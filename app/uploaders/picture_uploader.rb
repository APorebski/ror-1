class PictureUploader < CarrierWave::Uploader::Base

  include Cloudinary::CarrierWave

  process :convert => 'png'
  process :tags => ['post_picture']

  version :standard do
    process :resize_to_fill => [600, 480]
  end

  version :thumbnail do
    process :resize_to_fill => [200,120]
  end

  #def public_id
  #  return model.short_name
  #end

end
