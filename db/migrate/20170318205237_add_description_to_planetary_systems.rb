class AddDescriptionToPlanetarySystems < ActiveRecord::Migration[5.0]
  def change
    add_column :planetary_systems, :description, :string
  end
end
